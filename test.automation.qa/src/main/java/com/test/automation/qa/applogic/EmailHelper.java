package com.test.automation.qa.applogic;

public class EmailHelper extends DriverBasedHelper{

	public EmailHelper(ApplicationManager manager) {
		super(manager);
	}
	
	public Boolean isEmail(){
		manager.getNavigationHelper()
			.openEmailPage();
		pages.emailPage
			.clickSavedEmailLink();
		return pages.emailPage.getCountSavedEmail()>0;
	}

}
