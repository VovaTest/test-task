package com.test.automation.qa;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import com.test.automation.qa.applogic.ApplicationManager;
import com.test.automation.qa.model.User;

public class TestBase {

  public ApplicationManager app;

  @BeforeClass
  public void initTestSuite() throws IOException {
    app =  new ApplicationManager();
  }

  @AfterSuite
  public void tearDown() {
	  app.stop();
  }
  
  @DataProvider(name = "validUserData")
  public static Object[][] validUserDataGenerate() throws IOException{
	  return new Object[][]{
		{new User().setLogin("testtask1116@mailinator.com").setPassword("ZXCasdqwe123")},
		{new User().setLogin("testtask1118@mailinator.com").setPassword("ZXCasdqwe123")},
		{new User().setLogin("testtask1119@mailinator.com").setPassword("ZXCasdqwe123")}};	
		
  }
  
  @DataProvider(name = "invalidUserData")
  public static Object[][] invalidUserDataGenerate() throws IOException{
	  return new Object[][]{
		{new User().setLogin("testtask111655@mailinator.com").setPassword("ZXCasdqwe123")},
		{new User().setLogin("testtask1116@mailinator.com").setPassword("ZXCasdqwe12354")}};	
		
  }
}
