package com.test.automation.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class LoginPage extends InternalPage{

	public LoginPage(PageManager pages) {
		super(pages);
	}
	
	public LoginPage ensurePageLoaded(){
		super.ensurePageLoaded();
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//button[@class = 'btn btn-dark btn-lg btn-block']")));
		return this;
	}
	
	@FindBy(id = "loginEmail")
	private WebElement loginEmailField;
	
	@FindBy(id = "loginPassword")
	private WebElement loginPasswordField;
	
	@FindBy(xpath = "//button[@class = 'btn btn-dark btn-lg btn-block']")
	private WebElement loginButton;
	
	@FindBy(id = "loginError")
	private WebElement loginError;
	
	public LoginPage setLoginEmailField(String text){
		loginEmailField.clear();
		loginEmailField.sendKeys(text);
		return this;
	}
	
	public LoginPage setLoginPasswordField(String text){
		loginPasswordField.clear();
		loginPasswordField.sendKeys(text);
		return this;
	}
	
	public void clickLoginButton(){
		loginButton.click();
	}
	
	public Boolean getLoginErrorMessage(){
		return wait.until(ExpectedConditions.textToBePresentInElement(loginError, "invalid username or password"));
	}
	
	public Boolean isLoginPage(){
		boolean rezult;
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		if(driver.getCurrentUrl().contains("/login.jsp") 
				&& driver.findElement(By
						.xpath("//button[@class = 'btn btn-dark btn-lg btn-block']")).isDisplayed()){
			rezult =  true;
		}else{
			rezult = false;
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return rezult;
		
	}

}
