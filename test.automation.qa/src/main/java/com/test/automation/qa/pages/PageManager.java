package com.test.automation.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageManager {
	
	private WebDriver driver;
	
	public InternalPage internalPage;
	public LoginPage loginPage;
	public EmailPage emailPage;
	
	public PageManager(WebDriver driver){
		this.driver = driver;
		internalPage = initElements(new InternalPage(this));
		loginPage = initElements(new LoginPage(this));
		emailPage = initElements(new EmailPage(this));
	}
	
	private <T extends Page> T initElements(T page) {
		PageFactory.initElements(new DisplayedElementLocatorFactory(driver, 10), page);
		return page;
	}
	
	public WebDriver getWebDriver(){
		return driver;
	}

}
