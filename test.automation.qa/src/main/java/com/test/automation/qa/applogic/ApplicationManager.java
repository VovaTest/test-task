package com.test.automation.qa.applogic;

import java.io.IOException;
import java.util.concurrent.TimeUnit;





import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

import ru.stqa.selenium.factory.WebDriverFactory;

import com.test.automation.qa.util.PropertyLoader;



public class ApplicationManager {
	
	private WebDriver driver;
	private static String baseUrl;
	private static String gridHubUrl;
	private static Capabilities capabilities;
	
	private NavigationHelper navigationHelper;
	private EmailHelper emailHelper;
	private UserHelper userHelper;
	
	public ApplicationManager() throws IOException{
		
	    gridHubUrl = PropertyLoader.loadProperty("grid.url");
	    baseUrl = PropertyLoader.loadProperty("site.url");
	    if ("".equals(gridHubUrl)) {
	      gridHubUrl = null;
	    }
	    capabilities = PropertyLoader.loadCapabilities();
	    driver = WebDriverFactory.getDriver(gridHubUrl, capabilities);
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    getNavigationHelper().openUrlSite();
	    
	}
	
	public NavigationHelper getNavigationHelper(){
		if(navigationHelper == null){
			navigationHelper = new NavigationHelper(this);
		}
		return navigationHelper;
	}
	
	public EmailHelper getEmailHelper(){
		if(emailHelper == null){
			emailHelper = new EmailHelper(this);
		}
		return emailHelper;
	}
	
	public UserHelper getUserHelper(){
		if(userHelper == null){
			userHelper = new UserHelper(this);
		}
		return userHelper;
	}
	
	public WebDriver getWebDriver(){
		return driver;
	}

	public String getBaseUrl(){
		return baseUrl;
	}
	
	public void stop(){
		if (driver != null){
			driver.quit();
		}
	}

}
