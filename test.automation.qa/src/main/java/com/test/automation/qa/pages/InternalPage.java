package com.test.automation.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class InternalPage extends Page{

	public InternalPage(PageManager pageManager) {
		super(pageManager);
	}
	
	public InternalPage ensurePageLoaded(){
		super.ensurePageLoaded();
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//*[@class = 'nav navbar-nav navbar-right']")));
		return this;
	}
	
	@FindBy(xpath = "//*[@href = '/login.jsp']")
	private WebElement loginButtonMenu;
	
	@FindBy(xpath = "//*[@class = 'nav navbar-nav navbar-left']")
	private WebElement emailUser;
	
	@FindBy(xpath = "//*[@href = '/inbox2.jsp']")
	private WebElement emailButtonMenu;
	
	@FindBy(xpath = "//*[@onclick = 'logout();']")
	private WebElement logoutButtonMenu;
	
	public void clickLoginButton(){
		loginButtonMenu.click();
	}
	
	public void clickEmailButton(){
		emailButtonMenu.click();
	}
	
	public String getEmailUser(){
		return emailUser.getText();
	}
	
	public void clickLogout(){
		logoutButtonMenu.click();
		
	}

}
