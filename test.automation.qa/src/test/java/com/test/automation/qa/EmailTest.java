package com.test.automation.qa;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.automation.qa.model.User;

public class EmailTest extends TestBase{
	
	@BeforeMethod
	public void preCondition(Object[] data){
		app.getUserHelper()
			.login((User)data[0]);
		
	}
	
	@AfterMethod
	public void postCondition(){
		app.getUserHelper()
			.logout();
	}

	@Test(dataProvider = "validUserData", description="Save email presens test")
	public void isSaveEmailPresens(User user){
		Assert.assertTrue(app.getUserHelper()
				.isLoginUser(user));
		Assert.assertTrue(app.getEmailHelper()
				.isEmail());
	}
	
	
	

}
