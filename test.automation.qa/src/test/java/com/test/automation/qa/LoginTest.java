package com.test.automation.qa;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.test.automation.qa.model.User;

public class LoginTest extends TestBase{
	
	@AfterMethod
	public void postCondition(){
		app.getUserHelper()
			.logout();
	}
	
	@Test(dataProvider = "validUserData", description="Valid login test")
	public void loginTest(User user){
		app.getUserHelper()
			.login(user);
		Assert.assertTrue(app.getUserHelper()
				.isLoginUser(user));
	}
	
	@Test(dataProvider = "invalidUserData", description="Invalid login test")
	public void invalidLoginTest(User user){
		app.getUserHelper()
			.login(user);
		Assert.assertTrue(app.getUserHelper()
				.isNotLoginUser());
		
	}


}
