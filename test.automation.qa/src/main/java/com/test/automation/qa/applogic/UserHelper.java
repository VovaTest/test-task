package com.test.automation.qa.applogic;

import com.test.automation.qa.model.User;


public class UserHelper extends DriverBasedHelper{

	public UserHelper(ApplicationManager manager) {
		super(manager);
	}
	
	public void login(User user){
		manager.getNavigationHelper()
			.openLoginPage();
		pages.loginPage
			.setLoginEmailField(user.getLogin())
			.setLoginPasswordField(user.getPassword())
			.clickLoginButton();
	}
	
	public void logout(){
		if(!pages.loginPage.isLoginPage()){
			pages.internalPage
				.ensurePageLoaded()
				.clickLogout();
			pages.loginPage
				.ensurePageLoaded();
		}
		
	}
	
	public Boolean isLoginUser(User user){
		pages.emailPage
			.ensurePageLoaded();
		return pages.emailPage
				.ensurePageLoaded()
				.getEmailUser()
				.contains(user.getLogin());
		
	}
	
	public Boolean isNotLoginUser(){
		return pages.loginPage
				.ensurePageLoaded()
				.getLoginErrorMessage();	
	}

}
