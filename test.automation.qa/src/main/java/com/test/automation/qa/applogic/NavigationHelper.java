package com.test.automation.qa.applogic;

public class NavigationHelper extends DriverBasedHelper{
	
	private String baseUrl;

	public NavigationHelper(ApplicationManager manager) {
		super(manager);
		this.baseUrl = manager.getBaseUrl();
	}
	
	public void openLoginPage(){
		pages.internalPage
			.clickLoginButton();
		pages.loginPage
			.ensurePageLoaded();
	}
	
	public void openEmailPage(){
		pages.internalPage
			.clickEmailButton();
		pages.emailPage
			.ensurePageLoaded();
	}
	
	public void openUrlSite(){
		driver.get(baseUrl);
	}

}
