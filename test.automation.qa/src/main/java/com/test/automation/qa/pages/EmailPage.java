package com.test.automation.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EmailPage extends InternalPage{

	public EmailPage(PageManager pages) {
		super(pages);
	}
	
	public EmailPage ensurePageLoaded(){
		super.ensurePageLoaded();
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//*[contains(text(), 'Inbox:')]")));
		return this;
	}
	
	@FindBy(id = "saved_tabarea")
	private WebElement savedEmailLink;
	
	@FindBy(xpath = "//*[contains(@id, 'row_saved')]")
	private List<WebElement> listSavedEmail;
	
	public EmailPage clickSavedEmailLink(){
		savedEmailLink.click();
		return this;
	}
	
	public Integer getCountSavedEmail(){
		return listSavedEmail.size();
	}

}
